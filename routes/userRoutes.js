// import express and Router()
const express = require('express');
const router = express.Router();

const userControllers = require('../controllers/userControllers')
const {createUserController,getAllUserController,getSingleUserController,updateSingleUserController} = userControllers;

router.post('/', createUserController);
router.get('/', getAllUserController)
// get single user
router.get('/:id', getSingleUserController )
// update single user
router.put('/:id',updateSingleUserController)
module.exports = router;