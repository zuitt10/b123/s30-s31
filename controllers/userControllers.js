const User = require('../models/User')
module.exports.createUserController = (req,res)=>{

	console.log(req.body);
		User.findOne({username: req.body.username},(err,result)=>{

/*			console.log(err);*/
/*			console.log(result);*/

			if(result !== null && result.username === req.body.username){

				return res.send("Duplicate Username Found");

			} else {

					let newUser = new User({

						username: req.body.username,
						password: req.body.password,

					})

					newUser.save((saveErr,savedUser)=>{

						console.log(savedUser);

						if(saveErr){

							return console.error(saveErr);

						} else {

							return res.send("Registration Successful");

						}

					})


			}

		})

}

module.exports.getAllUserController = (req,res)=>{

	//Model.find() is a Mongoose method similar to MongoDB's find(). It is able to retrieve all documents that will match the criteria. 
	User.find({}).then(result =>  res.send(result)).catch(err => res.send(err))
}

module.exports.getSingleUserController = (req,res) =>{
	// Mongoose has a querry called findById() which works like find({_id:"id"})
	console.log(req.params.id)//the id passed from your url.
	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.updateSingleUserController = (req,res) =>{
	console.log(req.params.id)
	/*
		Model.findbyIdAndUpdate will do 2 things, first, look for the single item by its id, then add the update.

		Model.findByIdAndUpdate(id,{updates},{new:true})
	*/
	// updates object will contain the field and the value we want to update.
	let updates  = {

		username: req.body.username
		
	}
	User.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))

}