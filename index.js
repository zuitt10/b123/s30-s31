/*
	Create an expressjs api designated to port 4000.
	Create a new route with endpoint /hello and method GET.
		-should be able to respond with "Hello from our new Express Api!"
*/

const express = require("express");
const mongoose = require("mongoose");

const app = express();

const port = 4000;

//paste your mongodb connection string to the connect() method of mongoose module.
//change <password> to your db password
//change myFirstDatabase to todoList123
//MongoDB upon connection will create the todoList123 db once we created documents for it.
mongoose.connect("mongodb+srv://jericb123:Ravenger1@cluster0.9tqks.mongodb.net/todoList123?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
//notifications if connection to db is success or failed:
let db = mongoose.connection;
//console.error.bind(console,<message>) - print error in both terminal and browser
db.on("error",console.error.bind(console, "connection error."));
//Output a message in the terminal if the connection is successful
db.once("open", ()=> console.log("Connected to MongoDB"));

//Middleware - middleware, in expressjs context, are methods, functions that acts and adds features to your application.
//handle json data from our client.
app.use(express.json())

const taskRoutes = require('./routes/taskRoutes');
console.log(taskRoutes);
// A middle ware to group all of our routes starting
app.use('/tasks',taskRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);


//users

/*
	Activity

	Create a new schema for User. It should the following fields:
		username,password.
	The data types for field is a String.

	Create a new model out of your schema and save it in a variable called User

	Create a new POST method route to create a new user document:
		-endpoint: "/users"
		-This route should be able to check if there is a duplicate user document in the database based on username.
			-If there are duplicate users, send a message to the client:
			"Duplicate username found!"
			-Else, if there are no duplicates, create a newUser out of your User model and save() it.
				-In the anonymous function in save(), check if there is an error or not. IF there is an error, log the error in the console. Else, send a message to the client:
				"Successful Registration!"



	Pushing Instructions

	Go to Gitlab:
		-in your zuitt-projects folder and access b123 folder.
		-inside your b123 folder create a new repo called s30-s31
		-untick the readme option
		-copy the git url from the clone button of your s30-s31 repo.

	Go to Gitbash:
		-create a new file in s30-s31 folder
			-title: .gitignore
			-content: /node_modules
		-go to your b123/s30-s31 folder.
		-initialize s30-s31 folder as a local repo: git init
		-connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
		-add your updates to be committed: git add .
		-commit your changes to be pushed: git commit -m "includes expressJS mongoose activity 1"
		-push your updates to your online repo: git push origin master

	Go to Boodle:
		-copy the url of the home page for your s30-s31 repo (URL on browser not the URL from clone button) and link it to boodle:

		WD078-30 | Express.js - Data Persistence via Mongoose ODM

*/





app.get('/hello',(req,res)=>{

	res.send('Hello from our new Express Api!')
})


app.listen(port,()=>console.log(`Server running at port ${port}`))